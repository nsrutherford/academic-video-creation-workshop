\section{Video Editing}
\label{sec:video-editing}
    When recording your video, you may have parts that you want to shorten or cut out entirely.
    The editing stage is where you alter you clips to suit your narrative and export the final video.

    \subsection*{Learning Objectives}
        By the end of this section you should know how to:
        \begin{itemize}
            \item Download and open iMovie
            \item Perform some basic editing on a video
            \begin{itemize}
                \item Import a clip
                \item Add and edit video clips
                \item Add and edit audio clips
                \item Add titles and transitions
            \end{itemize}
            \item Export your final video
        \end{itemize}

    \subsection*{Preface}
        I present this workshop using iMovie, but there are a number of alternatives for other Operating Systems that are of similar quality (or better).
        Below is a couple of alternatives that I have used in the past and worked well.
        \begin{itemize}
            \item Hitfilm Express (free version available)
            \item DaVinci Resolve (free version available)
            \item Openshot (free and open-source)
            \item Final Cut Pro (paid)
            \item Adobe Premiere Pro (paid subscription)
        \end{itemize}
        This is not a comprehensive list, you will find many different packages that would fit your needs, these are just a few that I have used in the past.

    \subsection{Getting started with iMovie}
        iMovie comes bundled with MacOS, so you should already have it installed.
        If not, you can download it from the app store.
        The first time you open iMovie you will be greeted by a license agreement.
        If you are happy with the terms, click continue.
        You are now ready to use iMovie

        \paragraph{Creating a new project}
            When you open iMovie, you should see a blank screen containing a 'plus' symbol icon with the works "create new" underneath.
            Click this to create a new project for you video, selecting "Movie" as the type.
            Figure \ref{fig:imovie-ui} shows the iMovie user interface with different sections of this interface highlighted and explained below.

            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-2/imovie-ui.png}
                \caption{iMovie User Interface}
                \label{fig:imovie-ui}
            \end{figure}

            The yellow box will contain your 'timeline', this is used for structuring you video, e.g. sequence of video clips.
            Highighted in green is your media sources, these are the selection of clips you can use in your video.
            You can import clips using wither the import button on the top left (highlighted in brown), or by clicking on import media in the green section.
            The purple section shows the selection of media sources you can take clips from.
            In the orange section you will find a preview window of you clip.
            This preview is based on the contents of you timeline.
            Above the orange section in blue is some editing opens, such as brightness, cropping and colour correction.
            We will not cover all these options in the workshop, but they are worth highlighting here to encourage you to explore these later.
            Lastly, in the red section is options for adding audio tracks, titles, and adding clip transitions.

    \subsection{Importing a clip}
        Click on the "import media" button to open up the import interface shown in figure \ref{fig:imovie-import}.
        You use this menu to navigate to the location of you clip as you would in a file explorer.
        In the yellow section is the starting location to start you search.
        This will show a selection of files and folders in the green section that you can click on to either a) move to the folder, or b) select the clip.
        Selected clips appear in the pink section to preview.
        Finally, the blue section shows where the clip will be imported to within iMovie.
        If you are using the git repo accompanying this document, you can find some sample clips in \textit{academic-video-creation-workshop/samples/videos/}.
        Once you have found the clip you want to use, select "import" import on the bottom right of the interface menu.
        The clips should now appear in the media sources (green section in figure \ref{fig:imovie-ui}) for use in your final video.

        \begin{figure}[]
            \centering
            \includegraphics[width=\linewidth]{media/images/session-2/imovie-import.png}
            \caption{iMovie Import Menu}
            \label{fig:imovie-import}
        \end{figure}

    \subsection{Basic Editing with iMovie}
        \paragraph{Adding a clip to your timeline}
            Whenever you have imported you clips, you can begin to add them to your timeline by clicking and dragging them into the yellow section shown in figure \ref{fig:imovie-ui}.
            A blue box will show you where in the timeline your clip is going to be placed.
            Figure \ref{fig:imovie-clip-added} presents a reference of what your timeline can look like with an imported clip.

            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-2/imovie-add-clip.png}
                \caption{Example iMovie Timline with Clip Added}
                \label{fig:imovie-clip-added}
            \end{figure}
        
        \paragraph{Altering you Clip}
            You can navigate to different sections of you clip by moving your mouse cursor to any position along the timeline.
            The preview window will reflect your new position in the clip.
            You can split you clip by pressing \texttt{cmd + B} on your keyboard.
            This will allow you to trim out portions of the clip you do not want to keep and delete them.
            Figure \ref{fig:imovie-split-clip} shows an example of a clip that has been split.
            If you click on the split section (shown by the yellow border), you can remove it by pressing the delete (or backspace) key.
            
            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-2/imovie-split-clip.png}
                \caption{Example of splitting a clip}
                \label{fig:imovie-split-clip}
            \end{figure}

        \paragraph{Working with Audio}
            When recording your video, you will probably have some audio along with it.
            In iMovie you can detach the audio from the video into a separate timeline track, allowing you to alter the audio without modifying the video clip.
            You can do this by clicking on the clip you want the audio for, and pressing \texttt{alt + cmd + B} on your keyboard.
            The result should be similar to that of figure \ref{fig:imovie-split-audio}.
            You can split the audio channel using the same \texttt{cmd + B} hotkey as you did for video.
            With audio clips, you can also lower, or increase the volume of an audio clip by clicking and dragging the horizontal line (faint white line that can be seen in figure \ref{fig:imovie-split-audio} near the top of the audio clip) up or down.

            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-2/imovie-split-audio.png}
                \caption{Audio channel detached from video channel}
                \label{fig:imovie-split-audio}
            \end{figure}

        \paragraph{Titles and Transitions}
            You may want to add a title clip to you video, along with some transitions if you are using more than one recording.
            These settings can be found in the red region shown in figure \ref{fig:imovie-ui}.
            
            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-2/imovie-titles.png}
                \caption{Adding a title}
                \label{fig:imovie-title}
            \end{figure}

            To add a title, click and drag it from the title panel down to the location in your timeline you want it to appear.
            You can edit the text shown in this title by clicking on the text fields shown in the yellow section in figure \ref{fig:imovie-title}.

            Adding a transition follows the same procedure.
            In the transitions panel, click and drag the desired transition to the location in your timeline that you want it to play.
            For example, placing a 'fade-to-black' transition at the end of you video as shown in figure \ref{fig:imovie-transition}
            
            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-2/imovie-transition.png}
                \caption{Adding a fade-to-black Transition to the End of a Video}
                \label{fig:imovie-transition}
            \end{figure}

    \subsection{Exporting Your Final Video}
        Once you are happy with you video edit, you can export your project to a video file that you can share with your audience.
        To open the export menu, click on the export button shown in figure \ref{fig:imovie-export} and select "file".
        This will then open the export settings menu, which you can use to customise the quality of you video.
        The defaults should be fine, but figure \ref{fig:imovie-export-settings} provides an example of my settings that you can use for a reference.
        You can then click 'next', decide where you want you final video to be saved, and then wait for the rendering to finish.
        Note, the save window will close and you'll be able to see a file in the finder, but the video is not ready until you get a "Share Successful" notification from iMovie.

        \begin{figure}[]
            \centering
            \includegraphics[width=\linewidth]{media/images/session-2/imovie-export.png}
            \caption{Exporting a video}
            \label{fig:imovie-export}
        \end{figure}

        \begin{figure}[]
            \centering
            \includegraphics[width=\linewidth]{media/images/session-2/imovie-export-settings.png}
            \caption{Reference export settings}
            \label{fig:imovie-export-settings}
        \end{figure}

        Well done!
        You have successfully created you academic presentation.

