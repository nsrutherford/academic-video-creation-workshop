\section{Video Capture}
    The first stage of creation your video presentation is to capture some footage.
    As this workshop covers academic talks, I attempt to mimic the format one would expect from an academic presentation.
    That is, a screen capture containing a slide deck with (live) narration.

    \subsection*{Learning Objectives}
        By the end of this section you should know how to:
        \begin{itemize}
            \item Download and install OBS (or an alternative screen capture software package)
            \item Create a scene in OBS
            \item Configure your recording software to meet your requirements
            \item Record a simple live narration of your own presentation
        \end{itemize}

    \subsection*{Preface}
        While I use OBS in this workshop, the workflow should remain consistent across different recording packages.
        I have used OBS for a number of personal recording projects in the past, and have found it to meet my needs, but feel free to use an alternative \footnote{See \url{https://en.wikipedia.org/wiki/Comparison_of_screencasting_software} for a list of alternatives} if you are not a fan of OBS Studio.
        I tend to look for the following as requirements for a screencasting software package:
        \begin{itemize}
            \item Capture audio and video together in a single clip
            \item Export to .mp4 (or an alternative common format)
            \item Ability to add overlays to scene (e.g. webcam over slides)
            \item No watermark \footnote{Some free software packages will place a watermark on their free versions to persuade you to buy an upgraded version}
        \end{itemize}

    \subsection{Downloading OBS Studio}
        OBS Studio is a free open-source screen capture package that can be used for recording video locally, or streaming it to an online service like Twitch or Youtube.
        You can download OBS from \url{https://obsproject.com}.

        Click the download button for the Operating System that you are using, figure \ref{fig:obs-download} highlights the buttons area you need to select from.
        This will download an executable file that you can run.
        Follow the installation instructions for your platform.

        \begin{figure}[]
            \centering
            \includegraphics[width=\linewidth]{media/images/session-1/obs-download.png}
            \caption{Image of OBS Studio Download Buttons (highlighted in yellow)}
            \label{fig:obs-download}
        \end{figure}

        Once installed, run OBS Studio.
        You should then be presented with a blank OBS project window similar to the one shown in figure \ref{fig:obs-start} (with important parts of the user interface highlighted).
        Highlighted in yellow is you list of scenes (a collection of sources you want to record).
        The sources (devices/screens you want to capture) are highlighted in green.
        In the orange box you will see you audio sources such as microphones, these provide you with an idea of sound levels in your recording.
        On the right side of the image in blue are the control buttons for OBS.
        Lastly, underlined in pink are the informational outputs such as the recording indicator and frames per second (FPS).

        \begin{figure}[h]
            \centering
            \includegraphics[width=\linewidth]{media/images/session-1/obs-blank-window.png}
            \caption{Empty OBS project that you should see when you first start OBS}
            \label{fig:obs-start}
        \end{figure}

        You are now ready to create a scene!

    
    \subsection{Configuring Settings}
    \label{sec:obs-settings}
        In order to ensure everything is set-up as we want it before we hit record, it is worth first configuring you OBS software.
        You can open you settings in the user interface by clicking on the "Settings" button in the controls column (shown in blue in figure \ref{fig:obs-start})
        This window should look something like the one shown in figure \ref{fig:obs-settings}.

        \begin{figure}[]
            \centering
            \includegraphics[width=\linewidth]{media/images/session-1/obs-settings.png}
            \caption{OBS Studio Settings Interface}
            \label{fig:obs-settings}
        \end{figure}

        The first group of settings you should configure is \textit{output} with values similar to below for the \textit{recording} section:
        \begin{itemize}
            \item Mode $\rightarrow$ simple
            \item Recording Path $\rightarrow$ Browse $\rightarrow$ folder/to/save/your/recordings/in
            \item Recording Quality $\rightarrow$ Same as stream
            \item Recording Format $\rightarrow$ mp4
        \end{itemize}

        Next is video, something like below should work:
        \begin{itemize}
            \item Base (Canvas) Resolution $\rightarrow$ \textit{resolution of your primary display}
            \item Output (Scaled) Resolution $\rightarrow$ \textit{same as above}
            \item Downscale filter $\rightarrow$ Bicubic
            \item Common FPS Values $\rightarrow$ 30
        \end{itemize}

        These are just suggested values that work for me, you can experiment and find values that work for you as well!

        You should now be able to set-up your scenes and input sources to start recording.

    \subsection{Setting up a Scene}
        Before you can record you presentation, you need to tell OBS what sources you want to capture.
        All of the sources you tell OBS to capture come together to form your scene.
        You can have more than one scene that you can transition between using either the user interface or keyboard shortcuts.
        For example, you could have one scene for capturing you presentation, and second scene that is a full face webcam.

        The workflow for setting up a scene is given below, I use an example set-up of a single scene that records your main monitor using you laptop microphone for audio.

        \paragraph{Step 1 - Create a scene}
            OBS already has a single scene by default, so for now we do not need to add anything.
            However for you future reference, you can add a scene by clicking the add icon on the bottom left of the yellow box from figure \ref{fig:obs-start}.
            A scene can also be removed by clicking the minus icon beside the plus icon.
            You can also rename a scene to make it more intuitive during recording by right clicking on the scene $\rightarrow$ "rename".

        \paragraph{Step 2 - Create a source}
            A source in OBS is what the software records, e.g. one source can be your monitor and a second source can be you microphone.
            Each source belongs to a scene, and when toggled, OBS will capture input from all the sources at the same time.
            This allows you to have live commentary over you presentation as you record it.
            To add a source:
            \begin{itemize}
                \item Click on the plus icon on the bottom left of the green box highlighted in figure \ref{fig:obs-start}
                \item Select the type of source you want (display capture to capture you monitor)
                \item Name your source (main monitor)
                \item Select the display number (0 being the default)
                \item Untick show cursor if you want your mouse cursor to be hidden during recording
                \item Leave the crop at none for now
            \end{itemize}
            In the main window you should now see something similar to figure \ref{fig:obs-just-added}

            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-1/obs-just-added.png}
                \caption{Newly added display capture scene}
                \label{fig:obs-just-added}
            \end{figure}

            You may notice that your source is not properly sized as in figure \ref{fig:obs-just-added}.
            Sources can be resized by clicking and dragging on the red square box on the top left of you source.
            Note that you need to have clicked on the source in the preview window where it is visible.
            By clicking and dragging the red square you can resize the source to something like figure \ref{fig:obs-resized-source}

            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-1/obs-resized.png}
                \caption{Resized source}
                \label{fig:obs-resized-source}
            \end{figure}

            Should you want to crop out part of your source (e.g. to remove the start menu on windows, or the dock on Mac), you can do this using the same red square technique as before, but also hold the \texttt{alt} key as you click and drag.
            You will be able to recognise parts of you source that are clipped by the green line as shown in figure \ref{fig:obs-cropped}

            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-1/obs-cropped.png}
                \caption{Cropped Source (Indicated by Green Line Along the Bottom of the Source Frame)}
                \label{fig:obs-cropped}
            \end{figure}

            Other types of sources can be added using the same approach (clicking the plus) to make you scene more complex.
            You can use the up and down arrows near the plus icon to move sources 'frontwards' or 'backwards'.
            For example, if your main monitor is the top source, then everything else will be hidden behind it.
            Figure \ref{fig:obs-example-scene} shows an example OBS scene for recording you monitor with a webcam.
            Notice how the webcam source is listed above the main monitor source in the sources list.
            This places the webcam in front of the main display source, rather than hiding it behind.

            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-1/obs-example.png}
                \caption{Example OBS scene}
                \label{fig:obs-example-scene}
            \end{figure}

        \paragraph{Step 3 - Check you have audio}
            If you have a built-in microphone on your machine, OBS will include it by default.
            Otherwise you need to add a new audio input capture source.
            In order to verify you are capturing audio, check that the microphone is enabled (speaker is not red with an 'x' beside it), and is receiving input (movement in the volume mixer).
            Figure \ref{fig:obs-audio} shows an example microphone input.
            You can change the volume of this input by moving the slider.
            You should aim to keep the microphone volume within the green range.
            Microphone input that is in the yellow and especially red zone are going to sound unpleasant to your viewers.

            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-1/obs-audio.png}
                \caption{OBS audio mixer example}
                \label{fig:obs-audio}
            \end{figure}

        \paragraph{Step 4 - Configure settings (see section \ref{sec:obs-settings})}

        \paragraph{Step 5 - Hit Record!}
            Now that everything is configured and you have an audio and video source, you can start to record your video.
            To start recording hit the "record" button in the control column shown in clue on figure \ref{fig:obs-start}.
            You will know you are recording as the recording light will be on and a recording time will be shown.
            You can see an example of this in figure \ref{fig:obs-recording}.
            When you are done recording, click "stop recording", wait a moment, and then you will find your recording in the folder you defined during configuration as shown in section \ref{sec:obs-settings}.

            \begin{figure}[]
                \centering
                \includegraphics[width=\linewidth]{media/images/session-1/obs-recording.png}
                \caption{OBS recording indicator}
                \label{fig:obs-recording}
            \end{figure}

    At this stage you will no have a complete presentation recording, and you are now ready to edit the clip.
    You can find a guide on using iMovie for editing a clip in section \ref{sec:video-editing}.