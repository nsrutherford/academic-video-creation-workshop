# Academic Video Creation Workshop

Source material used for giving a one hour workshop on how to create video presentations for academic projects

## Usage:
I am providing this material under the [Creative Commons Attributed-ShareAlike 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).

This means you can share and adopt this material however you like, and in exchange you must attribute back to this repo, guide, or website, and distribute any materials you derive from my work with the same Creative Commons License.

This includes both commercial and non-commercial use.

## Structure:

| Folder | Description                                                   |
| -------| ------------------------------------------------------------- |
| Guide  | Written instructional guide for this workshop                 |
| Agenda | Sets out the structure for the one hour session               |
| Sample | Sample images and video clips you can use during the workshop |


## Acknowledgements 
Thanks to [Ragnar the Cat](https://cat.nsrutherford.com) for allowing me to use her as a model in the workshop samples.